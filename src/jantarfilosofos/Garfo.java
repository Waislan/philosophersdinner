package jantarfilosofos;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author waislan
 */
public class Garfo {
    private AtomicBoolean garfoOcupado = new AtomicBoolean(false);
    public int id;
    
    Garfo (int id){
        this.id = id;
    }
    
    public synchronized void pegar() throws InterruptedException{
        
        while(garfoOcupado.get()){
            System.out.println("Garfo "+id+" ocupado");
            this.wait();
        }
        
        this.garfoOcupado.set(true);
        
    }
    
    public synchronized void devolver(){
 
        this.garfoOcupado.set(false);
        notifyAll();
        
    }
    
}
