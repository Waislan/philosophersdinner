package jantarfilosofos;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author waislan
 */
public class Filosofo implements Runnable{
    
    public Garfo esquerdo;
    public Garfo direito;
    public int id;
    
    Filosofo(Garfo esquerdo, Garfo direito, int id){
        
        this.esquerdo = esquerdo;
        this.direito = direito;
        this.id = id;
        
    }
    
    public void pensar(){
        
        Random r = new Random();
        try {
            Thread.sleep(r.nextInt(500));
        } catch (InterruptedException ex) {
            Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void comer(){
        
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Filósofo "+id+" comendo.");
        
    }

    @Override
    public void run() {
        
        while (true) { 

            pensar();
            
            System.out.println("Filósofo "+id+" tentando pegar o garfo esquerdo.");
            
            try {
                esquerdo.pegar();
            } catch (InterruptedException ex) {
                Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Filósofo "+id+" pegou o garfo esquerdo.");
            
            System.out.println("Filósofo "+id+" tentando pegar o garfo direito.");
            
            try {
                direito.pegar();
            } catch (InterruptedException ex) {
                Logger.getLogger(Filosofo.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Filósofo "+id+" pegou o garfo direito.");
            
            comer();

            System.out.println("Filósofo "+id+" devolvendo o garfo esquerdo.");
            esquerdo.devolver();
            System.out.println("Filósofo "+id+" devolvendo o garfo direito.");
            direito.devolver();

        }
    }
    
}
